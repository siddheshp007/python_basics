# from array import *
# val = array('i', [2,4,1,12,4])
# print(val.buffer_info())


from array import *
arr = array("i", [])
size = int(input("Enter size of an array "))

for i in range(size):
    num = int(input("Enter value"))
    arr.append(num)

print("Final array is -- ", arr)

val = int(input("Enter value to check in array"))

k = 0
for j in arr:
    if j == val:
        print(val, " found at index ", k)
        break
    k += 1