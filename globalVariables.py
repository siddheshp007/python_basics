
a = 10

def test():
    # By using this, you can access only global variables, you can not use local variables
    #global a

    a = 5
    
    # this method is used to access all global variables declared
    x = globals()['a']

    # using this, you can change global variable, and also keep local as well
    globals()['a'] = 14
    print("inside function ", a)

test()
print("outside function ", a)
