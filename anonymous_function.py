from functools import *
num = [2,3,5,6,8,9,10]

def is_even(n):
    return n%2 == 0
#Filter
# even = list(filter(is_even ,num))
even = list(filter(lambda n: n%2 ==0 ,num))
# print(even)

#map
def getdoubles(n):
    return n*2
# doubles = list(map(getdoubles, even))
doubles = list(map(lambda n: n*2, even))
# print(doubles)

#reduce
def add_all(a,b):
    return a+b

# add = reduce(add_all, doubles)
add = reduce(lambda a,b: a+b, doubles)
print(add)