def divide(a,b):
    return a/b

# div = divide(2,4)

def smartdiv(func):
    def inner(a,b):
        if a < b:
            a,b = b,a
        return func(a,b)
    return inner

divide = smartdiv(divide)
div =  divide(2,4)
print(div)