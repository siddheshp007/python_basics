
def greet():
    print("Hi Good morning")


#greet()

# def add_sub(x,y):
#     add = x + y
#     sub = x - y
#     return add, sub
#
# a,b = add_sub(10,4)
# print(a,b)

#Keyword arguments
#
# def person(name, age):
#     print(name)
#     print(age)
#
# person(age=30, name="Siddhesh")

#Variable length arguments

# def sum(*b):
#     c = 0
#     for i in b:
#         c += i
#     print(c)
#
# sum(1,4,5,6,9)

#Keyword Variable length arguments

def person(name, **data):
    print(name)
    print(data)
    for i,j in data.items():
        print(i,":",j)

person("Siddhesh", age=30, city="Mumbai", mob=9879879879)