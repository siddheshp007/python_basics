# Print 5 X 5 stars
# * * * * *
# * * * * *
# * * * * *
# * * * * *
# * * * * *

# for i in range(5):
#     for j in range(5):
#         print(" * ", end="")
#     print()
# -------------------------------------------------------------

# Print
# *
# * *
# * * *
# * * * *
# * * * * *

for i in range(6):
    for j in range(i+1):
        print(" *", end="")
    print()

# -------------------------------------------------------------

# Print
# * * * * *
# * * * *
# * * *
# * *
# *

# for i in range(6,0,-1):
#     for j in range(i):
#         print(" *", end="")
#     print()