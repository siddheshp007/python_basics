class Car:
    def __init__(self, name, color):
        self.name = name
        self.color = color

    def getData(self):
        print("Car name:", self.name, " and color: ",self.color)


car1 = Car("Toyota", "black")
car2 = Car("Fords", "Red")

car1.getData()
car2.getData()


