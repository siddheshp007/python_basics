class Calculations:
    def add(self,a,b):
        return  a + b

# Creating object of class or instantiating class
obj1 = Calculations()
# 2 ways to call method of class
addition = Calculations.add(obj1,4,3) # first way
print(addition)

addition1 = obj1.add(4,3) # second way
print(addition1)