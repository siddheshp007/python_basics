#single inheritance

class A:
    def feature1(self):
        print("Inside feature 1")

    def feature2(self):
        print("Inside feature 2")


class B(A):
    def feature3(self):
        print("Inside feature 3")

    def feature4(self):
        print("Inside feature 4")


# a1 = A()
# a1.feature1()
# a1.feature2()

# print("-------------------------")
# b1 = B()
# b1.feature1()
# b1.feature2()
# b1.feature3()
# b1.feature4()

#------------------------------------------------------------

#multi level inheritance

class C(B):
    def feature5(self):
        print("Inside feature 5")

c1 = C()
c1.feature1()
c1.feature2()
c1.feature3()
c1.feature4()
c1.feature5()
