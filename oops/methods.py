class Student:

    school = "Shirodkar Highschool"

    #instance method
    def __init__(self, m1, m2, m3):     #marks
        self.m1 = m1
        self.m2 = m2
        self.m3 = m3

    # instance method
    def average(self):
        return (self.m1 + self.m2 + self.m3)/3

    # class method
    @classmethod    #used as decorators
    def info(cls):
        return cls.school

    @staticmethod
    def staticM():
        print("This is static method")


s1 = Student(40,55,70)
s2 = Student(55,75,80)

print(s1.average())
print(s2.average())
print(Student.info())

Student.staticM()