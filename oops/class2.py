class Computer:
    def __init__(self):
        self.name = "Siddhesh"
        self.age = 30

    def compare(self, other):
        if self.age == other.age:
            return True
        else:
            return False


# c1 = Computer()
# print(c1.name, c1.age)

c1 = Computer()
c1.age = 28
c2 = Computer()

if c1.compare(c2):
    print("Same age")
else:
    print("different age")