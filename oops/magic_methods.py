class Student:
    def __init__(self,m1, m2):
        self.m1 = m1
        self.m2 = m2

    # Operator overloading add  with changing operands
    def __add__(self, other):
        self.m = self.m1 + self.m2
        other.m = other.m1 + other.m2
        return self.m + other.m

    # Operator overloading greater than with changing operands
    def __gt__(self, other):
        self.m = self.m1 + self.m2
        other.m = other.m1 + other.m2
        if self.m > other.m:
            return True
        else:
            return False

s1 = Student(80,55)
s2 = Student(55,44)

s3 = s1 + s2
# print(s3)

if s1 > s2:
    print("s1 wins")
else:
    print("s2 wins")
