class Cars:
    wheels = 4              # Class Variables
    def __init__(self):
        self.name = "BMW"   #Instance variables
        self.mileage = 10   #Instance variables

c1 = Cars()
c2 = Cars()

c1.mileage=9

Cars.wheels = 5
# c1.wheels = 5

print(c1.name,c1.mileage, c1.wheels)
print(c2.name,c2.mileage, c2.wheels)