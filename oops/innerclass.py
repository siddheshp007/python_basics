
class Student:

    def __init__(self, name, rollno):
        self.name = name
        self.rollno = rollno
        self.lap = self.Laptop()

    def show(self):
        print(self.name, self.rollno,  self.lap.show())


    #inner class
    class Laptop:
        def __init__(self):
            self.brand = "Dell"
            self.ram = 8
            self.cpu = "i5"

        def show(self):
            print(self.brand, self.ram, self.cpu)

s1 = Student("Siddhesh", 101)
s2 = Student("Shruti", 102)

s1.show()
s2.show()