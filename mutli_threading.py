from threading import *
from time import  sleep

class Hi(Thread):
    def run(self):
        for i in range(5):
            print("Hi")
            sleep(1)


class Hello(Thread):
    def run(self):
        for i in range(5):
            print("Hello")
            sleep(1)


o1 = Hi()
o2 = Hello()

o1.start()
sleep(0.2)
o2.start()

o1.join()
o2.join()
print("Bye") #main thread