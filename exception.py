a = 5
b = 0

try:
    print("Resource open")
    print(a/b)

    k = int(input("enter number"))
    print(k)
except ZeroDivisionError as e:
    print("Division by zero")
except ValueError as e:
    print("Invalid value")
except Exception as e:
    print(e)

#It executes at end of this code
finally:
    print("Resource closed")