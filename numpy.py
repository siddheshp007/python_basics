from numpy import *

arr1 = array([
    [1,2,3,4,5,6],
    [7,8,9,0,11,23]
])

arr2 = arr1.flatten() //convert multi dim array to 1-D array
// array is having 12 elements
// reshapte(3,4) -> 3*4 ->12
arr3 = arr2.reshape(3,4) //it will create 3-D array with 4 elements

arr4 = arr2.reshape(2,2,3) // it will create 2, 2 - D array with 3 elements