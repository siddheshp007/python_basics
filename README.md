# Python basics

## Getting started with arithmetic operators
```
>> 10//3  # Output: 3
>>2*3  # Output: 6
>>2**3 # Output: 8
>>print(r'C:\docs\siddhes') # Output: Prints raw string
```

## _ (underscore) represent output of previous operations
```
>> x=5
>> x + 10 // Output: 15
>> _ + 5 // Output: 20
```

## String in python is immutable (Not changable)

## Lists

- [ ] Lists are mutable.
```
>> nums = [25, 10, 5, 15, 90]
>> nums.append(54) // [25, 10, 5, 15, 90, 54] -- (It will add element at last position)
>> nums.insert(2, 72) // [25, 10, 72, 5, 15, 90, 54] -- (It will add element at defined position)
>> nums.delete(15) // [25, 10, 72, 5, 90, 54] -- Delete value from list
>> nums.pop(1) // [25, 72, 5, 90, 54] -- It will remove value from given index
>> nums.pop() //[25, 72, 5, 90] -- It will remove last added element
>> del.nums[2:] //[25, 72] -- Removes multiple items using index specified
>> num.extend([29,14,33,53]) -- [25, 72, 29,14,33,53] //Add mulitple elements into list
```

## Tuples

- [ ] The tuple & lists are same but major difference is list is mutable but tuple is not mutable/immutable.
- [ ] The speed of iternation is more as compared to list, because its immutable.
```
>>tuple = (10, 3,44, 19)
>> tuple[0] // 10
```
## Sets
```
>> num = {1, 33, 21}
```

## Dictionary
- [ ] Key value pair
- [ ] Unique, immutable
```
>> data = {1 : "Siddhesh", 2: "Akshay"}
>> data[1] //Siddhesh 
>> data.get(1) //Siddhesh
>> data.get(3) // blank output -- Since there is no key present in dictionary
>> print(data.get(3)) //None -- Return none if key is not present
>> data(3, "Key not found") //Key not found -- If key not found it will print string provided
>> data.keys() //dict_keys([1,2]) -- Returns all keys
>> data.values() //dict_values([ "Siddhesh", "Akshat"]) -- Returns all values

>> keys = ["Siddhesh", "Akshay"]
>> values = ["Python", "Go"]
>> data = ditct(zip(keys, values)) //["Siddhesh":"Python", "Akshay":"Go"] -- similar like array_combine() php
>> data["Shruti"] = "PHP" //["Siddhesh":"Python", "Akshay":"Go", "Shruti": "PHP"] -- Add new key-value pair
>> del data["Akshay"] // ["Siddhesh":"Python", "Shruti": "PHP"] -- Akshay deleted
```
- [ ] Mutli-dimensional Dictionary:
```
>> prog = {'JS': 'Atom', 'CS': 'VS', 'Python':['Pycharm','Sublime'], 'Java':{'JSE' : 'Netbeans', 'JEEE': 'Eclipse'}}
>> prog['JS'] //Atom
>> prog['Python'] //['Pycharm','Sublime']
>> prog['Python'][1] //Sublime
>> prog['Java'] //{'JSE' : 'Netbeans', 'JEEE': 'Eclipse'}
>> prog['Java']['JSE'] // Netbeans
```

## How to get address of variable
```
>> num = 5
>> id(num) // 1823344590

>> a = 5
>> b = a
>> id(a) //1823344590
>> id(b) //1823344590
>> id(5) //1823344590
```
- [ ] if variables are having same values then they should have same address, hence python is more memory efficient.

## To declare constants, in python we use upper case letters.
```
>> PI = 3.14
>> type(PI) // <class 'float'> -- returns type of variable
```

## Data types in python
- [ ] None, Numeric, List, Set, Tuples, String, Range & Dictionary
- [ ] None (If there is a variable but no value assigned to it)
- [ ] Numeric (int, float, bool, complex) 

## Number system conversion
```
Binary = 0-1  bin(25) => 0b<number> --> b represents binary
Octal = 0-7	  oct(25) => 0o<number> --> o represents octal
Decimal = 0-9 dec(25) => 0d<number>  --> d represents decimal
HexaDecimal = 0-9,a-f => 0x<number>  --> x represents hexa decimal
```

## Bitwise operator

~ (tild operator) : 2's compliment (1's compliment + 1)
^ (ExOr)

## Math functions
```
>> import math as m //Represents alias
>> math.sqrt(25) //5.0
>> m.sqrt(25) //5.0
>> from math import sqrt, pow //Import selected packages from math
>> sqrt(25) //5.0
```

## User input/Prompt
- [ ] input() : prompts user to enter something
- [ ] eval(input()) : evaluates expressions entered by user
```
Arguments pass with filename
>> python <filename.py> <arg1> <arg2>
>> python add.py 5 3

Code: add.py
>>import sys
>> x = int(sys.argv[1])
>> y = int(sys.argv[2])
>> z = x + y
>> print(z) //returns 8
```

## For loop
- [ ] for i in range(10) //iterate from 0-9
- [ ] for i in range(11,20,1) //iterate from 11-19 with increment of 1 (excluding 20)
- [ ] for i in range(20,11,-1) //iterate from 20-10 with decrement of 1 (excluding 11)

## Array
```
//Two methods can be used to import array
>>import array as arr
>>from arrat import *

--> To define array
>> val = array('i', [2,4,1,12,4]) //this array is having values of type signed integer(i)
>> print(val, buffer_info()) //(12387439, 5) -- prints addresss, size of an array

--> Copying array from existing array
>> val = array('i', [2,4,1,12,4]) 
>> newArr = array(val.typecode, (i for i in val)) 
```

## Numpy (To work with multi-dimensional array in python)
```
1> array()
>> from numpy import *
>> arr = array([2,4,1,12,4], int) //type is optional

2>linspace(<start>, <end>, <steps>) 
>> linespace(0,16,15) //0 - 15

--> Copy array
>> arr1 = array([1,2,3])
>> arr2 = arr1.view() //Shallow copy(If parent array is modified then it's copy will also change). It will print same array values but different addresses
>> arr2 = arr1.copy() //Deep copy(parent array & copied array are independent)

>> arr1.dtype //returns type of values present inside the array
>> arr1.ndim //returns dimensions of an array 2/3/4
>> arr1.shape //returns rows & columns i.e. 2 rows, 3 columns (2,3)
>> arr1.size //returns dimensions of an array 2/3/4
>> arr1.flatten() //convers multi-dimensional array to 1-dimensional array
```

## Pass by reference is applicable only for list not for string & int.


## Variable Lenght argument in function
```
>> def sum(*b): // * -> Indicates no fixed length of arguments passed
>> c = 0
>> for i in b:
>>	c += i
>> print(c)
>> sum(1,4,5,6,9) // print 25
```

## Keyword Variable Lenght argument in function
```
>> def person(name, **data): // ** -> Indicates no fixed length of arguments passed with keyword
>> print(name)
>> print(data)
>> for i,j in data.items():
>> 	print(i,":",j)
>> person("Siddhesh", age=30, city="Mumbai", mob=9879879879)
```

## Recursion Limit
- [ ]  It is 1000 by defualt
```
sys.getrecursionlimit() --> 1000
sys.setrecursionlimit(2000) --> 2000
```

## Anonymous/Lambda functions
```
>> f = lambda a,b : a + b  //lambda keyword is used for anonymous function having single line statement.
>> print(f(5,3)) // 8

**Filter
//Check even numbers with filter
>> num = [2,4, 5,6,7,3,9]
>> evens = list(filter(lambda n : n%2 == 0 , num))

**Map 
//It will take data and perform operation on it and return. eg. check even nos and add 2 to every item and the return all elements.
>> doubles = list(map(lambda n : n*2 , evens))

**Reduce
from functools import *
add = reduce(lambda a,b: a+b, doubles)
```

## Decorator

- [ ]  A decorator is a design pattern in Python that allows a user to add new functionality to an existing object without modifying its structure.
- [ ]  Decorators are usually called before the definition of a function you want to decorate.

## Special Variable __name__
- [ ] It is the starting point of execution just like golang main()
```
>>print(__name__) // __main__
```

- [ ] If we use same variable in 2 files, it will print __main__ for the file we run and it will show module name for imported file.
```
file1 --> helper.py
>> print("Hello ", __name__)

file2 --> demo.py
>> from helper import *
>>print("demo says ", __name__) 

//Output
 demo says __main__
 Hello helper

```

## Object Oriented Programing
- [ ] python supports procedure oriented, object oriented & functional programming(lambda).

## Class & Object
```
class Calculations:
    def add(self,a,b):
        return  a + b

# Creating object of class or instantiating class
obj1 = Calculations()

# 2 ways to call method of class
addition = Calculations.add(obj1,4,3) # first way
print(addition)

addition1 = obj1.add(4,3) # second way
print(addition1)
```
## Special Variable __init__
- [ ] It works similar like __constructor() in php. 
- [ ] It gets called automatically as soon as object of class is created.

## Methods
```
class Cars:
    wheels = 4              # Class Variables
    def __init__(self):
        self.name = "BMW"   #Instance variables
        self.mileage = 10   #Instance variables

c1 = Cars()
c2 = Cars()

c1.mileage=9

Cars.wheels = 5
# c1.wheels = 5

print(c1.name,c1.mileage, c1.wheels)
print(c2.name,c2.mileage, c2.wheels)
```

## Constructor/init methods in multi-level inheritance
- [ ] There are 3 classes in mulit-level inheritance and each one has constructor. If I called child class using its object then it will not execute parent's constructor but its own constructor
```
class A:
	def __init__(self):
		print("Init A")

class B(A):
	super().__init__()  //Init A -- this method is used to access parent's init methods
	def __init__(self):
		print("Init B")

b = B() //It will print Init B instead of Init A
```

- [ ] In case of multiple inheritance, it will MRO (Method Resolution order) i.e. left to right. If we use super() method to access the parent's method, it will return the left class method.

## Duck Type 
- [ ] It is very similar to interfaces.

## Polymorphism
- [ ] Python does not supports method overloading.

## Abstraction
- [ ] Abstract class should have atleast 1 abstract method
- [ ] Abstract methods does not have defination, it only has declaration.

```
To use abstract in python, we have to use:
from abc import ABC,abstractmethod  //ABC - Abstract Based Classes
```

## Errors
- [ ] There are 3 types of errors: Compile time error, Logical error, Run time error.

## Multi-threading
- [ ] To achieve this, we must use run() method and to execute the thread, we have to use start() method.
```
from threading import *
from time import  sleep

class Hi(Thread):
    def run(self):
        for i in range(5):
            print("Hi")
            sleep(1)

class Hello(Thread):
    def run(self):
        for i in range(5):
            print("Hello")
            sleep(1)
o1 = Hi()
o2 = Hello()

o1.start()
sleep(0.2) //Use this to avoid collision between threads
o2.start()

#join() tells main thread to wait until all threads completes their execution.
o1.join()
o2.join()
print("Bye") //main thread
```
- [ ] In the above program, there are 3 threads used (o1, o2 & main thread)