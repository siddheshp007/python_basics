#show count of even & odd numbers from list using user input

user_input = input("Enter Numbers to check count of even & odd ")
list = user_input.split()
print(list)

def count(data):
    even = 0
    odd = 0
    for i in data:
        i = int(i)
        if i%2 == 0:
            even += 1
        else:
            odd += 1
    return even, odd


evenC, oddC = count(list)
#print("evenCount:", evenC, " oddCount:", oddC)
print("evenCount: {}, oddCount: {}". format(evenC,oddC))